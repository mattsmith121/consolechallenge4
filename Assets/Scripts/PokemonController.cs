using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PokemonController : Singleton<PokemonController>
{
    [System.Serializable]
    class Pokemon
    {
        public string name;
        public SpriteData sprites;
    }

    [System.Serializable]
    class SpriteData
    {
        public string front_default;
    }

    public Text pokemonNameText;
    public Image pokemonImage;
    private Pokemon pokemon;

    void Start() {
        pokemon = null;
        pokemonNameText.text = "";
    }

    public bool isPokemon() {
        return pokemon != null;
    }

    public void GetPokemon(string indexString) {
        int index = -1;
        if (!int.TryParse(indexString, out index)) {
            Debug.LogWarning("Pokemon index not an integer");
            return;
        }

        string url = "https://pokeapi.co/api/v2/pokemon/" + index;

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();

        StreamReader reader = new StreamReader(response.GetResponseStream());
        string jsonResponse = reader.ReadToEnd();

        pokemon = JsonUtility.FromJson<Pokemon>(jsonResponse);

        pokemonNameText.text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(pokemon.name.ToLower());
        StartCoroutine(GetSprite(pokemon.sprites.front_default));

    }

    IEnumerator GetSprite(string spriteURL) {
        UnityWebRequest www = UnityWebRequest.Get(spriteURL);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success) {
            Debug.Log("Error getting sprite");
            Debug.Log(www.error);
        }
        else {
            // Or retrieve results as binary data
            byte[] results = www.downloadHandler.data;
            ConvertBinaryToSprite(results);
        }
    }

    private void ConvertBinaryToSprite(byte[] spriteData) {
        Texture2D texture = new Texture2D(1,1);
        texture.LoadImage(spriteData);
        Sprite sprite = Sprite.Create(texture, new Rect(Vector2.zero, new Vector2(96f, 96f)), new Vector2(0.5f, 0.5f), 100.0f);
        pokemonImage.sprite = sprite;
    }
}

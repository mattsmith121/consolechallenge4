using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Leaderboards : MonoBehaviour
{
    public PlayfabManager playfabManager;
    public PokemonController pokemonController;
    public Button CatchButton;
    public Text numCatches;
    int highscore = 0;

    void Start()
    {
        CatchButton.enabled = false;
    }

    public void OnLogin() {
        CatchButton.enabled = true;

        GetHighScore();
    }

    public void GetHighScore() {
        PlayFabClientAPI.GetPlayerStatistics(new GetPlayerStatisticsRequest {
            StatisticNames = new List<string>() { "pokemon_caught" }
        },
        OnGetHighScore,
        error => { Debug.Log("Error Getting Highscore"); }
        );
    }

    private void OnGetHighScore(GetPlayerStatisticsResult result) {
        if (result.Statistics.Count > 0) {
            highscore = result.Statistics[0].Value;
            numCatches.text = highscore.ToString();
        } else {
            numCatches.text = "0";
        }
    }

    public void PostHighScore() {
        // Need pokemon to catch a pokemon
        if (!pokemonController.isPokemon()) {
            return;
        }
        PlayFabClientAPI.UpdatePlayerStatistics(new UpdatePlayerStatisticsRequest {
            Statistics = new List<StatisticUpdate> {
                    new StatisticUpdate { StatisticName = "pokemon_caught", Value = (highscore + 1) }
            }
        },
        OnUpdatePlayerStatistics,
        OnPlayfabError
        );
    }

    private void OnUpdatePlayerStatistics(UpdatePlayerStatisticsResult result) {
        highscore++;
        numCatches.text = highscore.ToString();
        Debug.Log("User stats updated");
    }

    private void OnPlayfabError(PlayFabError error) {
        Debug.Log("Error on stat update/get");
    }
}

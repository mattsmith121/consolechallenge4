using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;

public class PlayfabManager : Singleton<PlayfabManager>
{
    public enum LoginState
    {
        Startup,
        Success,
        Failed
    }
    public LoginState state = LoginState.Startup;
    public string PlayerID = "";
    public bool createNewPlayer = false;
    public UserSettings userSettings;
    public Leaderboards leaderboards;

    void Awake() {
        PlayerID = PlayerPrefs.GetString("PlayerID", "");
        if (string.IsNullOrEmpty(PlayerID) || createNewPlayer) {
            PlayerID = System.Guid.NewGuid().ToString();
            PlayerPrefs.SetString("PlayerID", PlayerID);
        }
    }

    void Start() {
        LoginWithCustomIDRequest request = new LoginWithCustomIDRequest { CustomId = PlayerID, CreateAccount = true };
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
    }

    private void OnLoginSuccess(LoginResult result) {
        state = LoginState.Success;
        userSettings.OnLogin();
        leaderboards.OnLogin();
        Debug.Log("Congratulations, you have logged into Playfab!");
    }

    private void OnLoginFailure(PlayFabError error) {
        state = LoginState.Failed;
        Debug.LogWarning("Something went wrong logging into Playfab");
        Debug.LogWarning("here's some debug info: " + error.GenerateErrorReport());
    }
}

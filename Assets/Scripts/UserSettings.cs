using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserSettings : MonoBehaviour
{
    public Button UpdateUserNameButton;
    public InputField UserNameText;

    void Start() {
        UpdateUserNameButton.enabled = false;
        UserNameText.enabled = false;
    }

    public void OnLogin() {
        // Enable UI
        if (!UpdateUserNameButton.enabled && PlayfabManager.Instance.state == PlayfabManager.LoginState.Success) {
            UpdateUserNameButton.enabled = true;
            UserNameText.enabled = true;
        }

        // Get Display Name
        PlayFabClientAPI.GetPlayerProfile(new GetPlayerProfileRequest(),
            OnGetPlayerProfile,
            error => {
                Debug.Log("Error getting player profile");
            });
    }

    public void UpdateUserName() {
        PlayFabClientAPI.UpdateUserTitleDisplayName(new UpdateUserTitleDisplayNameRequest() { DisplayName = UserNameText.text },
            result => {
                Debug.Log("User name changed successfully");
            },
            error => {
                Debug.Log("Failed to update Username");
            });
    }

    public void OnGetPlayerProfile(GetPlayerProfileResult result) {
        UserNameText.text = result.PlayerProfile.DisplayName;
    }
}
